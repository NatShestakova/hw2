import sys
import random

def main(argv):
    file_name = argv[1]
    lines_count = int(argv[2])
    
    # opening file from argv[1]
    with open(file_name, 'w') as f:
        for i in range(lines_count):
            # passport number generating
            passport = random.randint(12344321, 43211234)
            # year generating
            year = random.randint(1900, 2007)
            # value_type generating 
            value_type = random.randint(0, 1)

            # value generating 
            value = 0
            if value_type == 1:
                # number of trips
                value = random.randint(0, 50)
            else:
                # salary
                value = random.randint(30000, 200000)

            # line in csv-format
            line = str(passport) + ',' + str(year) + ',' + str(value_type) + ',' + str(value) + '\n'
            f.write(line)

if __name__ == "__main__":
    main(sys.argv)
