import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaPairRDD;

import java.time.Year;
import java.lang.*;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import scala.Tuple2;
import scala.Tuple3;

public final class CitizenStatistics
{
    private static int[] ageCategories = {18, 30, 50};

    public static void main(String[] args)
    {
        // ----------------------------------- Spark RDD ---------------------------------------------------------------
        // setting configuration
        SparkConf conf = new SparkConf().setAppName("CitizenStatistics");

        // getting spark context using our configuration
        JavaSparkContext sparkContext = new JavaSparkContext(conf);

		// reading data from text file
        JavaRDD<String> allDataRdd = sparkContext.textFile(args[0]);

        // parsing strings to tuples
        JavaRDD<Tuple3<Integer, Integer, Integer>> parsedData = allDataRdd.map(CitizenStatistics::parseStringToTuple);
        // filtering parsed data from invalid age category = -1
        JavaRDD<Tuple3<Integer, Integer, Integer>> filteredData = parsedData.filter(t -> t._1() >= 0);

        // getting average value of salary (type = 0)
        JavaPairRDD<Integer,Double> averageSalary = getAverageValue(filteredData, 0);
        // getting average value of trips (type = 1)
        JavaPairRDD<Integer,Double> averageTripsCount = getAverageValue(filteredData, 1);

        // ---------------------------------- Kafka --------------------------------------------------------------------

        // assign topicName to string variable
        String topicName = "CitizenStatistics";

        // create instance for properties to access producer configs
        Properties props = new Properties();

        // assign localhost id
        props.put("bootstrap.servers", "localhost:9092");

        // set acknowledgements for producer requests.
        props.put("acks", "all");

        // if the request fails, the producer can automatically retry,
        props.put("retries", 0);

        // specify buffer size in config
        props.put("batch.size", 16384);

        // reduce the no of requests less than 0
        props.put("linger.ms", 1);

        // the buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);

        props.put("key.serializer","org.apache.kafka.common.serialization.StringSerializer");

        props.put("value.serializer","org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<>(props);

        // collecting rdd to list
        List<Tuple2<Integer, Double>> averageSalaryList = averageSalary.collect();
        List<Tuple2<Integer, Double>> averageTripsCountList = averageTripsCount.collect();
        List<String> results = new Vector<>();

        for (int i = 0; i < averageSalaryList.size(); ++i)
        {
            // concatenating data from tuple to string
            String kafkaValue = averageSalaryList.get(i)._1.toString() +
                    "," + averageSalaryList.get(i)._2.toString() +
                    "," + averageTripsCountList.get(i)._2.toString();

            // sending record to kafka
            producer.send(new ProducerRecord<>(topicName, kafkaValue));
            results.add(kafkaValue);
        }

        // printing results on console
        for (String resultLine : results)
        {
            System.out.println(resultLine);
        }

        // stopping
        producer.close();
        sparkContext.stop();
    }

    private static JavaPairRDD<Integer, Double> getAverageValue(JavaRDD<Tuple3<Integer, Integer, Integer>> data, int type)
    {
        // filtering data by value type
        JavaRDD<Tuple3<Integer, Integer, Integer>> filteredData = data.
                filter(t -> t._2() == type);

        // mapping values
        JavaPairRDD<Integer, Tuple2<Integer, Integer>> mappedData = filteredData.
                mapToPair(t -> new Tuple2<>(t._1(), new Tuple2<>(t._3() ,1)));

        // calculating sum of all values and number of values
        JavaPairRDD<Integer, Tuple2<Integer, Integer>> reducedData = mappedData.
                reduceByKey((a, b) -> new Tuple2<>(a._1() + b._1(), a._2() + b._2()));

        // calculating average value
        return reducedData.mapToPair(t -> new Tuple2<>(t._1(), t._2()._1() / t._2()._2().doubleValue()));
    }


    public static Tuple3<Integer, Integer, Integer> parseStringToTuple(String sourceString) {
        // default tuple that contains ageCategory, valueType (salary or number of trips), value by valueType
        Tuple3<Integer, Integer, Integer> defaultTuple = new Tuple3<>(-1, -1, -1);

        // splitting input string by spaces
        String splittedString[] = sourceString.split(",");
        int year, valueType, value;

        if (splittedString.length != 4) {
            return defaultTuple;
        }

        try {
            // first parameter contains passport (ignoring of splittedString[0])

            // second contains year of birth
            year      = Integer.parseInt(splittedString[1]);

            // third contains valueType (salary or number of trips)
            valueType = Integer.parseInt(splittedString[2]);

            // fourth contains value
            value     = Integer.parseInt(splittedString[3]);
        } catch (NumberFormatException e) {
            return defaultTuple;
        }

        // getting current year
        int currentYear = Year.now().getValue();
        // calculating citizen age
        int age = currentYear - year;

        int ageCategory = -1;
        int lastAgeCategory = ageCategories.length - 1;

        // assignment age category provided by ageCategories array
        for (int i = 0; i <= lastAgeCategory; i++)
        {
            if (age <= ageCategories[i] && age > 0) {
                ageCategory = i;
                break;
            }
        }

        if (age > ageCategories[lastAgeCategory]) {
            ageCategory = lastAgeCategory + 1;
        }

        return new Tuple3<>(ageCategory, valueType, value);
    }
}
