import org.junit.Test;
import scala.Tuple3;

import static junit.framework.TestCase.assertEquals;

public class CitizenStatisticsTest {

    // testing valid string parsing
    @Test
    public void testValidStringParsing() {
        String source = "1234321,1994,0,50000";
        Tuple3<Integer, Integer, Integer> parsedData = CitizenStatistics.parseStringToTuple(source);

        // testing age category
        assertEquals(1, parsedData._1().intValue());
        // testing type of value
        assertEquals(0, parsedData._2().intValue());
        // testing value (salary or number of trips)
        assertEquals(50000, parsedData._3().intValue());
    }

    // testing invalid string parsing
    @Test
    public void testInvalidShortStringParsing() {
        String source = "1234321,1994";
        Tuple3<Integer, Integer, Integer> parsedData = CitizenStatistics.parseStringToTuple(source);

        // testing age category
        assertEquals(-1, parsedData._1().intValue());
        // testing type of value
        assertEquals(-1, parsedData._2().intValue());
        // testing value (salary or number of trips)
        assertEquals(-1, parsedData._3().intValue());
    }

    // testing invalid string parsing
    @Test
    public void testInvalidYearParsing() {
        String source = "1234321,2094,0,5000";
        Tuple3<Integer, Integer, Integer> parsedData = CitizenStatistics.parseStringToTuple(source);

        // testing age category
        assertEquals(-1, parsedData._1().intValue());
        // testing type of value
        assertEquals(0, parsedData._2().intValue());
        // testing value (salary or number of trips)
        assertEquals(5000, parsedData._3().intValue());
    }
}
